## #Prerequisite Installed
- [Nginx](https://gist.github.com/nd3w/8017f2e0b8afb44188e733d2ec487deb)(Recommended) or Apache Webserver
- [PHP 7.4](https://gist.github.com/nd3w/8017f2e0b8afb44188e733d2ec487deb)
- [Composer](https://getcomposer.org/download/)
- [Git](https://git-scm.com/downloads)
## Install
Open Terminal on Linux and run:
```
git clone https://github.com/LQTam/ecommerce.git
cd laravel-8-ecommercial
cp .env.example .env
```

Open `.env` file and update these variables to fit to you.

```
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=ecommerce_db
DB_USERNAME=root
DB_PASSWORD=
```

Add your [mailtrap](https://mailtrap.io/) info to testing mail
```
MAIL_MAILER=smtp
MAIL_HOST=mailhog
MAIL_PORT=1025
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null
MAIL_FROM_ADDRESS=null
```

And Run
```
composer install
php artisan key:generate
php artisan storage:link
php artisan migrate
php artisan db:seed
php artisan serve
```

Run `php artisan queue:work` if you want to check mail after ordered.

## Test Account
**User Account**: user@gmail.com/user@gmail.com

**Admin Account**: admin@admin.com/admin@admin.com