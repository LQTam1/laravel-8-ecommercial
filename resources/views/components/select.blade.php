@props(['options' => []])

<select {{$attributes}} class="form-control">
    @if(gettype($options) === 'object')
        {{$slot}}
        @foreach($options as $option)
            <option value="{{$option->id}}">{{$option->name}}</option>
        @endforeach
    @elseif(gettype($options) === 'array')
        {{$slot}}
        @foreach($options as $option)
            <option value="{{$option['key']}}">{{$option['value']}}</option>
        @endforeach
    @endif
</select>
