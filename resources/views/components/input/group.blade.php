@props(['label' => 'Default Label','error'=> '', 'wireIgnore', 'required' => false])
<div>
    <!-- Walk as if you are kissing the Earth with your feet. - Thich Nhat Hanh -->
    <div class="form-group @error($error) has-error @enderror">
        <label for="" class="col-md-4 control-label">{{$label}}
            @if($required)
                <span
                    class="text-danger">*</span>
            @endif
        </label>
        @isset($wireIgnore)
            <div class="col-md-4" wire:ignore>
                {{ $slot }}
                @error($error)
                <span class="text-danger">{{ $message }}</span>
                @enderror
            </div>
        @else
            <div class="col-md-4">
                {{ $slot }}
                @error($error)
                <span class="text-danger">{{ $message }}</span>
                @enderror
            </div>
        @endif
    </div>
</div>
