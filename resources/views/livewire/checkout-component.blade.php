<main id="main" class="main-site">
    <style>
        .wrap-address-billing .row-in-form input[type=password] {
            font-size: 13px;
            line-height: 19px;
            display: inline-block;
            height: 43px;
            padding: 2px 20px;
            width: 100%;
            border: 1px solid #e6e6e6;
        }

    </style>

    <div class="container">

        <div class="wrap-breadcrumb">
            <ul>
                <li class="item-link"><a href="#" class="link">home</a></li>
                <li class="item-link"><span>login</span></li>
            </ul>
        </div>
        <div class=" main-content-area">
            <form action="#" wire:submit.prevent='placeOrder'>
                <div class="row">
                    <div class="col-md-12">
                        <div class="wrap-address-billing">
                            <h3 class="box-title">Billing Address</h3>

                            <div class='billing-address'>
                                <p class="row-in-form @error('firstname') has-error @enderror">
                                    <label for="fname">first name<span>*</span></label>
                                    <input type="text" name="fname" value="" placeholder="Your name"
                                        wire:model='firstname'>
                                    @error('firstname')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </p>
                                <p class="row-in-form @error('lastname') has-error @enderror">
                                    <label for="lname">last name<span>*</span></label>
                                    <input type="text" name="lname" value="" placeholder="Your last name"
                                        wire:model='lastname'>
                                    @error('lastname')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </p>
                                <p class="row-in-form @error('email') has-error @enderror">
                                    <label for="email">Email Addreess:</label>
                                    <input type="email" name="email" value="" placeholder="Type your email"
                                        wire:model='email'>
                                    @error('email')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </p>
                                <p class="row-in-form @error('mobile') has-error @enderror">
                                    <label for="phone">Phone number<span>*</span></label>
                                    <input type="number" name="phone" value="" placeholder="10 digits format"
                                        wire:model='mobile'>
                                    @error('mobile')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </p>
                                <p class="row-in-form @error('line1') has-error @enderror">
                                    <label for="add">Line1:</label>
                                    <input type="text" name="add" value="" placeholder="Street at apartment number"
                                        wire:model='line1'>
                                    @error('line1')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </p>
                                <p class="row-in-form">
                                    <label for="add">Line2:</label>
                                    <input type="text" name="add" value="" placeholder="Street at apartment number"
                                        wire:model='line2'>
                                </p>
                                <p class="row-in-form @error('province') has-error @enderror">
                                    <label for="province">Province<span>*</span></label>
                                    <input type="text" name="province" value="" placeholder="Province"
                                        wire:model='province'>
                                    @error('province')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </p>
                                <p class="row-in-form @error('city') has-error @enderror">
                                    <label for="city">Town / City<span>*</span></label>
                                    <input type="text" name="city" value="" placeholder="City name" wire:model='city'>
                                    @error('city')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </p>
                                <p class="row-in-form @error('country') has-error @enderror">
                                    <label for="country">Country<span>*</span></label>
                                    <input type="text" name="country" value="" placeholder="Country"
                                        wire:model='country'>
                                    @error('country')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </p>
                                <p class="row-in-form @error('zipcode') has-error @enderror">
                                    <label for="zip-code">Postcode / ZIP:</label>
                                    <input type="number" name="zip-code" value="" placeholder="Your postal code"
                                        wire:model='zipcode'>
                                    @error('zipcode')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </p>
                                <p class="row-in-form fill-wife">
                                    <label class="checkbox-field">
                                        <input name="different-add" id="different-add" value="1" type="checkbox"
                                            wire:model='is_shipping_different'>
                                        <span>Ship to a different address?</span>
                                    </label>
                                </p>
                            </div>
                        </div>
                    </div>

                    @if ($is_shipping_different)
                        <div class="col-md-12">
                            <div class="wrap-address-billing">
                                <h3 class="box-title">Shipping Address</h3>
                                <div class='billing-address'>
                                    <p class="row-in-form @error('shipping_firstname') has-error @enderror">
                                        <label for="fname">first name<span>*</span></label>
                                        <input type="text" name="fname" value="" placeholder="Your name"
                                            wire:model='shipping_firstname'>
                                        @error('shipping_firstname')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </p>
                                    <p class="row-in-form @error('shipping_lastname') has-error @enderror">
                                        <label for="lname">last name<span>*</span></label>
                                        <input type="text" name="lname" value="" placeholder="Your last name"
                                            wire:model='shipping_lastname'>
                                        @error('shipping_lastname')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </p>
                                    <p class="row-in-form @error('shipping_email') has-error @enderror">
                                        <label for="email">Email Addreess:</label>
                                        <input type="email" name="email" value="" placeholder="Type your email"
                                            wire:model='shipping_email'>
                                        @error('shipping_email')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </p>
                                    <p class="row-in-form @error('shipping_mobile') has-error @enderror">
                                        <label for="phone">Phone number<span>*</span></label>
                                        <input type="number" name="phone" value="" placeholder="10 digits format"
                                            wire:model='shipping_mobile'>
                                        @error('shipping_mobile')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </p>
                                    <p class="row-in-form @error('shipping_line1') has-error @enderror">
                                        <label for="add">Line1:</label>
                                        <input type="text" name="add" value="" placeholder="Street at apartment number"
                                            wire:model='shipping_line1'>
                                        @error('shipping_line1')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </p>
                                    <p class="row-in-form ">
                                        <label for="add">Line2:</label>
                                        <input type="text" name="add" value="" placeholder="Street at apartment number"
                                            wire:model='shipping_line2'>
                                    </p>
                                    <p class="row-in-form @error('shipping_country') has-error @enderror">
                                        <label for="country">Country<span>*</span></label>
                                        <input type="text" name="country" value="" placeholder="Country"
                                            wire:model='shipping_country'>
                                        @error('shipping_country')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </p>
                                    <p class="row-in-form @error('shipping_province') has-error @enderror">
                                        <label for="province">Province<span>*</span></label>
                                        <input type="text" name="province" value="" placeholder="Province"
                                            wire:model='shipping_province'>
                                        @error('shipping_province')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </p>
                                    <p class="row-in-form @error('shipping_city') has-error @enderror">
                                        <label for="city">Town / City<span>*</span></label>
                                        <input type="text" name="city" value="" placeholder="City name"
                                            wire:model='shipping_city'>
                                        @error('shipping_city')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </p>
                                    <p class="row-in-form @error('shipping_zipcode') has-error @enderror">
                                        <label for="zip-code">Postcode / ZIP:</label>
                                        <input type="number" name="zip-code" value="" placeholder="Your postal code"
                                            wire:model='shipping_zipcode'>
                                        @error('shipping_zipcode')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </p>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>

                <div class="summary summary-checkout">
                    <div class="summary-item payment-method">
                        <h4 class="title-box">Payment Method</h4>

                        @if ($payment_method === 'card')
                            <div class="wrap-address-billing">
                                @if (session()->has('stripe_error'))
                                    <div class="alert alert-danger" role="alert">{{ session()->get('stripe_error') }}
                                    </div>
                                @endif
                                <p class="row-in-form @error('card_no') has-error @enderror">
                                    <label for="card_no">Card Number:</label>
                                    <input type="number" name="card_no" value="" placeholder="Card Number"
                                        wire:model='card_no'>
                                    @error('card_no')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </p>
                                <p class="row-in-form @error('exp_month') has-error @enderror">
                                    <label for="exp_month">Expiry Month:</label>
                                    <input type="number" name="exp_month" value="" placeholder="MM"
                                        wire:model='exp_month'>
                                    @error('exp_month')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </p>
                                <p class="row-in-form @error('exp_year') has-error @enderror">
                                    <label for="exp_year">Expiry Year:</label>
                                    <input type="number" name="exp_year" value="" placeholder="YY"
                                        wire:model='exp_year'>
                                    @error('exp_year')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </p>
                                <p class="row-in-form @error('cvc') has-error @enderror">
                                    <label for="cvc">CVC:</label>
                                    <input type="password" name="cvc" value="" placeholder="CVC" wire:model='cvc'>
                                    @error('cvc')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </p>
                            </div>

                        @endif
                        <div class="choose-payment-methods">
                            <label class="payment-method">
                                <input name="payment-method" id="payment-method-bank" value="cod" type="radio"
                                    wire:model='payment_method'>
                                <span>Cash On Delivery</span>
                                <span class="payment-desc">Order Now Pay on Delivery</span>
                            </label>
                            <label class="payment-method">
                                <input name="payment-method" id="payment-method-visa" value="card" type="radio"
                                    wire:model='payment_method'>
                                <span>Debit / Credit Card</span>
                                <span class="payment-desc">There are many variations of passages of Lorem Ipsum
                                    available</span>
                            </label>
                            {{-- <label class="payment-method">
                                <input name="payment-method" id="payment-method-paypal" value="paypal" type="radio"
                                    wire:model='payment_method'>
                                <span>Paypal</span>
                                <span class="payment-desc">You can pay with your credit</span>
                                <span class="payment-desc">card if you don't have a paypal account</span>
                            </label> --}}
                            @error('payment_method')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>

                        @if (session()->has('checkout'))
                            <p class="summary-info grand-total"><span>Grand Total</span> <span
                                    class="grand-total-price">${{ session()->get('checkout')['total'] }}</span>
                            </p>
                        @endif

                        <button type="submit" class='btn btn-medium'>Place order now</button>
                    </div>

                    <div class="summary-item shipping-method">
                        <h4 class="title-box f-title">Shipping method</h4>
                        <p class="summary-info"><span class="title">Flat Rate</span></p>
                        <p class="summary-info"><span class="title">
                                {{-- @if (session()->has('checkout') && ($checkout = session()->get('checkout')))
                                    @if ($checkout['type'] && $checkout['type'] === 'percent')
                                        Percent
                                        {{ $checkout['disscount'] }}%
                                    @endif
                                    @else
                                    Fixed ${{ $checkout['disscount'] }}
                                @endif --}}
                                Fixed $0
                            </span></p>
                    </div>
                </div>
            </form>
        </div>
        <!--end main content area-->
    </div>
    <!--end container-->

</main>
