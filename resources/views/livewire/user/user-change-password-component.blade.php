<div>
    <div class="container" style="padding: 30px 0">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Change Password</div>
                    <div class="panel-body">
                        @if (session()->has('password_success'))
                            <div class="alert alert-success" role="alert">{{ session()->get('password_success') }}
                            </div>
                        @endif
                        @if (session()->has('password_error'))
                            <div class="alert alert-danger" role="alert">{{ session()->get('password_error') }}</div>
                        @endif
                        <form action="" class="form-horizontal" wire:submit.prevent='changePassword'>
                            <div class="form-group @error('current_password') has-error @enderror">
                                <label for="" class="col-md-4 control-label">Current Password</label>
                                <div class="col-md-4">
                                    <input type="password" placeholder="Current Password" name="current_password"
                                        class="form-control input-md" wire:model='current_password'>
                                    @error('current_password')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group @error('password') has-error @enderror">
                                <label for="" class="col-md-4 control-label">New Password</label>
                                <div class="col-md-4">
                                    <input type="password" placeholder="New Password" name="password"
                                        class="form-control input-md" wire:model='password'>
                                    @error('password')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group @error('password_confirmation') has-error @enderror">
                                <label for="" class="col-md-4 control-label">Confirm Password</label>
                                <div class="col-md-4">
                                    <input type="password" placeholder="Confirm Password" name="password_confirmation"
                                        class="form-control input-md" wire:model='password_confirmation'>
                                    @error('password_confirmation')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-md-4 control-label"></label>
                                <div class="col-md-4">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
