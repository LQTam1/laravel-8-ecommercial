<div>
    <div class="container" style='padding: 30px 0'>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Manager Home Categories</div>
                    <div class="panel-body">
                        @if (session()->has('message'))
                            <div class="alert alert-success" role="alert">{{ session()->get('message') }}</div>
                        @endif
                        <form action="" class="form-horizontal" wire:submit.prevent='update'>
                            <div class="form-group @error('selected_categories') has-error @enderror">
                                <label for="" class="col-md-4 control-label">Choose Categories</label>
                                <div class="col-md-4" wire:ignore>
                                    <select name="categories[]" class="sel_categories form-control" multiple="multiple"
                                        wire:model='selected_categories'>
                                        @foreach ($categories as $category)
                                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('selected_categories')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group @error('no_of_products') has-error @enderror">
                                <label for="" class="col-md-4 control-label">No. Of Products</label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control input-md" wire:model='no_of_products' />
                                    @error('no_of_products')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-md-4 control-label"></label>
                        <div class="col-md-4">
                            <button type='submit' class="btn btn-primary">Save</button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

@push('scripts')
    <script>
        $(document).ready(function() {
            $('.sel_categories').select2();
            $('.sel_categories').on('change', function(e) {
                let data = $('.sel_categories').select2('val');
                @this.set('selected_categories', data)
            })
        })
    </script>
@endpush
