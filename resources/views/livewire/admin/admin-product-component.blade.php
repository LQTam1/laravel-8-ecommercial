<div>
    <style>
        nav svg {
            height: 20px;
        }

        nav .hidden {
            display: block !important;
        }

    </style>
    <div class="container" style='padding: 30px 0'>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-6">All Products</div>
                            <div class="col-md-6">
                                <a href="{{ route('admin.products.create') }}" class='btn btn-success pull-right'>Add
                                    New</a>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        @if (session()->has('message'))
                            <div class="alert alert-success" role="alert">{{ session()->get('message') }}</div>
                        @endif
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Image</th>
                                    <th>Name</th>
                                    <th>Stock</th>
                                    <th>Price</th>
                                    <th>Sale Price</th>
                                    <th>Category</th>
                                    <th>Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($products as $product)
                                    <tr>
                                        <td>{{ $product->id }}</td>
                                        <td>
                                            <a href="{{ route('product.details', ['slug' => $product->slug]) }}">
                                                <img src="{{ asset('assets/images/products') }}/{{ $product->image }}"
                                                    alt="{{ $product->name }}" width='60'>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="{{ route('product.details', ['slug' => $product->slug]) }}">
                                                {{ $product->name }}
                                            </a>
                                        </td>
                                        <td>{{ $product->stock_status }}</td>
                                        <td>{{ $product->regular_price }}</td>
                                        <td>{{ $product->sale_price }}</td>
                                        <td>{{ $product->category->name }}</td>
                                        <td>{{ $product->created_at }}</td>
                                        <td>
                                            <a
                                                href="{{ route('admin.products.edit', ['product_slug' => $product->slug]) }}"><i
                                                    class='fa fa-edit fa-2x text-info'></i></a>
                                            @if ($product->id === $product_id)
                                                <a href="#" wire:click.prevent='destroy({{ $product->id }})'
                                                    style='margin-left:10px;' class='text-success'><i
                                                        class="fa fa-check fa-2x "></i>Sure?</a>
                                                <a href="#" class='text-danger' style='margin-left:10px;'
                                                    wire:click.prevent='confirmDestroyProduct()'><i
                                                        class="fa fa-times fa-2x "></i>Cancel</a>
                                            @else
                                                <a href="#" style='margin-left: 10px'
                                                    wire:click.prevent="confirmDestroyProduct({{ $product->id }})"><i
                                                        class='fa fa-times fa-2x text-danger'></i></a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $products->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
