<div>
    <div class="container" style='padding: 30px 0'>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-6">Edit Coupon</div>
                            <div class="col-md-6">
                                <a href="{{ route('admin.coupons.index') }}" class='btn btn-success pull-right'>All
                                    Coupons</a>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        @if (session()->has('message'))
                            <div class="alert alert-success" role="alert">{{ session()->get('message') }}</div>
                        @endif
                        <form class="form-horizontal" wire:submit.prevent="update">
                            <div class="form-group @error('code') has-error @enderror">
                                <label for="" class="col-md-4 control-label">Coupon Code</label>
                                <div class="col-md-4">
                                    <input type="text" placeholder="Coupon Code" wire:model='code'
                                        class="form-control input-md">
                                    @error('code')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group @error('type') has-error @enderror">
                                <label for="" class="col-md-4 control-label">Coupon Type</label>
                                <div class="col-md-4">
                                    <select wire:model='type' class='form-control'>
                                        <option value="fixed">Fixed</option>
                                        <option value="percent">Percent</option>
                                    </select>
                                    @error('type')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group @error('value') has-error @enderror">
                                <label for="" class="col-md-4 control-label">Coupon Value</label>
                                <div class="col-md-4">
                                    <input type="text" placeholder="Coupon Value" wire:model='value'
                                        class="form-control input-md">
                                    @error('value')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group @error('cart_value') has-error @enderror">
                                <label for="" class="col-md-4 control-label">Cart Value</label>
                                <div class="col-md-4">
                                    <input type="text" placeholder="Cart Value" wire:model='cart_value'
                                        class="form-control input-md">
                                    @error('cart_value')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group @error('expiry_date') has-error @enderror">
                                <label for="" class="col-md-4 control-label">Expiry Date</label>
                                <div class="col-md-4" wire:ignore>
                                    <input type="text" id='expiry_date' placeholder="Expiry Date"
                                        wire:model='expiry_date' class="form-control input-md">
                                    @error('expiry_date')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-md-4 control-label"></label>
                                <div class="col-md-4">
                                    <button type="submit" class="btn btn-primary">Update</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <script>
        $(function() {
            $('#expiry_date').datetimepicker({
                    format: 'Y-MM-DD'
                })
                .on("dp.change", function(e) {
                    const expiry_date = $('#expiry_date').val();
                    @this.set('expiry_date', expiry_date);
                })
        })
    </script>
@endpush
