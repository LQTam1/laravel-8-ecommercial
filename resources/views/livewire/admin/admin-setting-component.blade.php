<div>
    <div class="container" style='padding: 30px 0'>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Settings</div>
                    <div class="panel-body">
                        @if (session()->has('setting_message'))
                            <div class="alert alert-success" role="alert">{{ session()->get('setting_message') }}
                            </div>
                        @endif
                        <form class="form-horizontal" wire:submit.prevent='saveSettings'>
                            <div class="form-group @error('email') has-error @enderror">
                                <label for="" class="control-label col-md-4">Email</label>
                                <div class="col-md-4">
                                    <input type="email" placeholder="Email" class="form-control input-md"
                                        wire:model='email'>
                                    @error('email')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group @error('phone') has-error @enderror">
                                <label for="" class="control-label col-md-4">Phone</label>
                                <div class="col-md-4">
                                    <input type="text" placeholder="Phone" class="form-control input-md"
                                        wire:model='phone'>
                                    @error('phone')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group @error('phone2') has-error @enderror">
                                <label for="" class="control-label col-md-4">Phone2</label>
                                <div class="col-md-4">
                                    <input type="text" placeholder="Phone2" class="form-control input-md"
                                        wire:model='phone2'>
                                    @error('phone2')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group @error('address') has-error @enderror">
                                <label for="" class="control-label col-md-4">Address</label>
                                <div class="col-md-4">
                                    <input type="text" placeholder="Address" class="form-control input-md"
                                        wire:model='address'>
                                    @error('address')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group @error('map') has-error @enderror">
                                <label for="" class="control-label col-md-4">Map</label>
                                <div class="col-md-4">
                                    <input type="text" placeholder="Map" class="form-control input-md" wire:model='map'>
                                    @error('map')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group @error('tweeter') has-error @enderror">
                                <label for="" class="control-label col-md-4">Tweeter</label>
                                <div class="col-md-4">
                                    <input type="text" placeholder="Tweeter" class="form-control input-md"
                                        wire:model='tweeter'>
                                    @error('tweeter')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group @error('facebook') has-error @enderror">
                                <label for="" class="control-label col-md-4">Facebook</label>
                                <div class="col-md-4">
                                    <input type="text" placeholder="Facebook" class="form-control input-md"
                                        wire:model='facebook'>
                                    @error('facebook')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group @error('pinterest') has-error @enderror">
                                <label for="" class="control-label col-md-4">Pinterest</label>
                                <div class="col-md-4">
                                    <input type="text" placeholder="Pinterest" class="form-control input-md"
                                        wire:model='pinterest'>
                                    @error('pinterest')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group @error('instagram') has-error @enderror">
                                <label for="" class="control-label col-md-4">Instagram</label>
                                <div class="col-md-4">
                                    <input type="text" placeholder="Instagram" class="form-control input-md"
                                        wire:model='instagram'>
                                    @error('instagram')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group @error('youtube') has-error @enderror">
                                <label for="" class="control-label col-md-4">Youtube</label>
                                <div class="col-md-4">
                                    <input type="text" placeholder="Youtube" class="form-control input-md"
                                        wire:model='youtube'>
                                    @error('youtube')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="" class="control-label col-md-4"></label>
                                <div class="col-md-4">
                                    <button class="btn btn-primary" type="submit">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
