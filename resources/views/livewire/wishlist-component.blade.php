<main id="main" class="main-site left-sidebar">
    <style>
        .product-wishlist {
            position: absolute;
            top: 10%;
            z-index: 99;
            right: 30px;
            text-align: right;
        }

        .product-wishlist .fa {
            color: #cbcbcb;
            font-size: 32px;
        }

        .product-wishlist .fa:hover {
            color: #ff2832;
        }

        .product-wishlist .fa.fill-heart {
            color: #ff2832;
        }

        .alert-success+.row>.no-item {
            margin-top: 30px;
            margin-left: 15px;
        }

    </style>

    <div class="container">

        <div class="wrap-breadcrumb">
            <ul>
                <li class="item-link"><a href="/" class="link">home</a></li>
                <li class="item-link"><span>Wishlist</span></li>
            </ul>
        </div>

        @if (session()->has('success_message'))
            <span class="alert alert-success" role="alert">{{ session()->get('success_message') }}</span>
        @endif
        <div class="row">
            @if (Cart::instance('wishlist')->content()->count() > 0)
                <ul class="product-list grid-products equal-container">
                    @foreach (Cart::instance('wishlist')->content() as $item)
                        <li class="col-lg-3 col-md-6 col-sm-6 col-xs-6 ">
                            <div class="product product-style-3 equal-elem ">
                                <div class="product-thumnail">
                                    <a href="{{ route('product.details', ['slug' => $item->model->slug]) }}"
                                        title="{{ $item->model->name }}">
                                        <figure><img
                                                src="{{ asset('assets/images/products') }}/{{ $item->model->image }}"
                                                alt="{{ $item->model->name }}"></figure>
                                    </a>
                                </div>
                                <div class="product-info">
                                    <a href="{{ route('product.details', ['slug' => $item->model->slug]) }}"
                                        class="product-name"><span>{{ $item->model->name }}</span></a>
                                    <div class="wrap-price"><span
                                            class="product-price">${{ $item->model->regular_price }}</span></div>
                                    <a href="#" class="btn add-to-cart"
                                        wire:click.prevent="moveToCart('{{ $item->rowId }}')">Add
                                        To Cart</a>
                                    <div class="product-wishlist">
                                        <a href="#"
                                            wire:click.prevent='removeFromWishlist({{ $item->model->id }})'><i
                                                class="fa fa-heart fill-heart"></i></a>
                                    </div>
                                </div>
                            </div>
                        </li>
                    @endforeach
                </ul>
            @else
                <h4 class='no-item'>No item in Wishlist</h4>
            @endif
        </div>
    </div>
</main>
