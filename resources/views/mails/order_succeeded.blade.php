@component('mail::message')
# Order Succeeded

Your order has been placed!

@component('mail::table')
| Customer Name       | Phone         | Address  |
| ------------- |:-------------:| --------:|
| {{ $invoice['full_name'] }}      | {{ $invoice['phone'] }}      | {{ $invoice['line1'] . ' ' . $invoice['city']. ' ' . $invoice['province']. ' ' . $invoice['country']}}      |
@endcomponent

@component('mail::table')
| Product Name       | Price         | Quantity  | Subtotal
| ------------- |:-------------:| :--------:| --------:|
@foreach ($invoice['items'] as $orderItem)
| {{$orderItem->product->name}}      | ${{ $orderItem->price }}      | {{ $orderItem->quantity }}      | ${{$orderItem->price*$orderItem->quantity}}
@endforeach
@endcomponent

@if ($invoice['discount'] > 0)
    <h3>Discount: {{$invoice['discount']}}</h3>
@endif
<h3>Tax: {{$invoice['tax']}}</h3>
<h3>Total: {{$invoice['total']}}</h3>

@component('mail::button', ['url' => route('user.orders.show',$invoice['order_id'])])
    View Order
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
