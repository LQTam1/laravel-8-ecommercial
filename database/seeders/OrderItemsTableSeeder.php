<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class OrderItemsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('order_items')->delete();
        
        \DB::table('order_items')->insert(array (
            0 => 
            array (
                'id' => 1,
                'product_id' => 3,
                'order_id' => 1,
                'price' => '112.00',
                'quantity' => 6,
                'created_at' => '2021-10-05 20:41:01',
                'updated_at' => '2021-10-05 20:41:01',
                'rvstatus' => 0,
            ),
            1 => 
            array (
                'id' => 2,
                'product_id' => 18,
                'order_id' => 2,
                'price' => '379.00',
                'quantity' => 1,
                'created_at' => '2021-10-05 20:42:16',
                'updated_at' => '2021-10-05 20:43:47',
                'rvstatus' => 1,
            ),
            31 => 
            array (
                'id' => 32,
                'product_id' => 1,
                'order_id' => 32,
                'price' => '12.00',
                'quantity' => 9,
                'created_at' => '2021-10-05 20:54:36',
                'updated_at' => '2021-10-05 20:54:36',
                'rvstatus' => 0,
            ),
        ));
        
        
    }
}