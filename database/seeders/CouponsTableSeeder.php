<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class CouponsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('coupons')->delete();
        
        \DB::table('coupons')->insert(array (
            0 => 
            array (
                'id' => 1,
                'code' => 'D20P',
                'type' => 'percent',
                'value' => '20.00',
                'cart_value' => '50.00',
                'expiry_date' => '2021-10-30',
                'created_at' => '2021-10-05 20:50:19',
                'updated_at' => '2021-10-05 20:50:19',
            ),
            1 => 
            array (
                'id' => 2,
                'code' => 'D100',
                'type' => 'fixed',
                'value' => '100.00',
                'cart_value' => '100.00',
                'expiry_date' => '2021-10-29',
                'created_at' => '2021-10-05 20:50:46',
                'updated_at' => '2021-10-05 20:50:46',
            ),
        ));
        
        
    }
}