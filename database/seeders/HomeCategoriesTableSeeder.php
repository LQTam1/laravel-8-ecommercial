<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class HomeCategoriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('home_categories')->delete();
        
        \DB::table('home_categories')->insert(array (
            0 => 
            array (
                'id' => 1,
                'sel_categories' => '1,2,3,5',
                'no_of_products' => 8,
                'created_at' => '2021-08-12 15:17:54',
                'updated_at' => '2021-10-05 20:48:35',
            ),
        ));
        
        
    }
}