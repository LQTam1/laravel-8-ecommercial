<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class OrdersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('orders')->delete();

        \DB::table('orders')->insert(array(
            0 =>
            array(
                'id' => 1,
                'user_id' => 1,
                'subtotal' => '672.00',
                'discount' => '0.00',
                'tax' => '141.12',
                'total' => '813.12',
                'firstname' => 'Christian Weber',
                'lastname' => 'Stacey Mathews',
                'mobile' => '45',
                'email' => 'gozutiqasi@mailinator.com',
                'line1' => 'Ea dolore sed autem ',
                'line2' => 'Sit dolor ullam num',
                'city' => 'Nisi sint natus in u',
                'province' => 'Sapiente voluptatibu',
                'country' => 'Aliquid veritatis fu',
                'zipcode' => '81174',
                'status' => 'delivered',
                'is_shipping_different' => 0,
                'created_at' => '2021-10-05 20:41:01',
                'updated_at' => '2021-10-05 20:45:04',
                'delivered_date' => '2021-10-06',
                'canceled_date' => '2021-10-06',
            ),
            1 =>
            array(
                'id' => 2,
                'user_id' => 2,
                'subtotal' => '379.00',
                'discount' => '0.00',
                'tax' => '79.59',
                'total' => '458.59',
                'firstname' => 'Raven Elliott',
                'lastname' => 'Rudyard Brooks',
                'mobile' => '69',
                'email' => 'vewuruquri@mailinator.com',
                'line1' => 'Quis sint libero al',
                'line2' => 'Consectetur laborum',
                'city' => 'Non sit magni non o',
                'province' => 'Quo dolorem ut fugia',
                'country' => 'Perferendis consequa',
                'zipcode' => '71296',
                'status' => 'canceled',
                'is_shipping_different' => 0,
                'created_at' => '2021-10-05 20:42:16',
                'updated_at' => '2021-10-05 20:44:27',
                'delivered_date' => '2021-10-06',
                'canceled_date' => '2021-10-06',
            ),
            31 =>
            array(
                'id' => 32,
                'user_id' => 1,
                'subtotal' => '8.00',
                'discount' => '100.00',
                'tax' => '1.68',
                'total' => '9.68',
                'firstname' => 'Howard Santana',
                'lastname' => 'Ronan Schmidt',
                'mobile' => '66',
                'email' => 'fameri@mailinator.com',
                'line1' => 'A dolorum temporibus',
                'line2' => 'Sit recusandae Aut ',
                'city' => 'Omnis impedit in co',
                'province' => 'Minim architecto sim',
                'country' => 'Architecto omnis ut ',
                'zipcode' => '12216',
                'status' => 'ordered',
                'is_shipping_different' => 0,
                'created_at' => '2021-10-05 20:54:36',
                'updated_at' => '2021-10-05 20:54:36',
                'delivered_date' => NULL,
                'canceled_date' => NULL,
            ),
        ));
    }
}
