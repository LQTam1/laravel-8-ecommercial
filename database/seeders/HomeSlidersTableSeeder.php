<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class HomeSlidersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('home_sliders')->delete();
        
        \DB::table('home_sliders')->insert(array (
            0 => 
            array (
                'id' => 1,
                'title' => 'title',
                'subtitle' => 'subtitle',
                'price' => '344',
                'link' => 'http://google.com',
                'image' => '1633466802.jpg',
                'status' => 1,
                'created_at' => '2021-10-05 20:46:42',
                'updated_at' => '2021-10-05 20:46:42',
            ),
            1 => 
            array (
                'id' => 2,
                'title' => 'title 1',
                'subtitle' => 'subtitle 1',
                'price' => '669',
                'link' => 'http://youtube.com',
                'image' => '1633466832.jpg',
                'status' => 1,
                'created_at' => '2021-10-05 20:47:12',
                'updated_at' => '2021-10-05 20:47:12',
            ),
            2 => 
            array (
                'id' => 3,
                'title' => 'title 2',
                'subtitle' => 'subtitle 2',
                'price' => '669',
                'link' => 'http://facebook.com',
                'image' => '1633466843.jpg',
                'status' => 1,
                'created_at' => '2021-10-05 20:47:23',
                'updated_at' => '2021-10-05 20:47:23',
            ),
        ));
        
        
    }
}