<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class TransactionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('transactions')->delete();
        
        \DB::table('transactions')->insert(array (
            0 => 
            array (
                'id' => 1,
                'user_id' => 1,
                'order_id' => 1,
                'mode' => 'cod',
                'status' => 'pending',
                'created_at' => '2021-10-05 20:41:01',
                'updated_at' => '2021-10-05 20:41:01',
            ),
            1 => 
            array (
                'id' => 2,
                'user_id' => 2,
                'order_id' => 2,
                'mode' => 'cod',
                'status' => 'pending',
                'created_at' => '2021-10-05 20:42:16',
                'updated_at' => '2021-10-05 20:42:16',
            ),
            2 => 
            array (
                'id' => 3,
                'user_id' => 1,
                'order_id' => 32,
                'mode' => 'cod',
                'status' => 'pending',
                'created_at' => '2021-10-05 20:54:36',
                'updated_at' => '2021-10-05 20:54:36',
            ),
        ));
        
        
    }
}