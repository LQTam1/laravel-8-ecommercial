<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('settings')->delete();
        
        \DB::table('settings')->insert(array (
            0 => 
            array (
                'id' => 1,
                'email' => 'contact@gmail.com',
                'phone' => '0123456789',
                'phone2' => '012234567',
                'address' => 'DA,NoWhere,UK',
                'map' => 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d13047.57951641755!2d-98.45103496029458!3d35.15924232798904!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x87adb30af7b76acb%3A0xae2aac995c0e1ebe!2sNowhere%2C%20Oklahoma%2073038%2C%20Hoa%20K%E1%BB%B3!5e0!3m2!1svi!2s!4v1628188951697!5m2!1svi!2s',
                'tweeter' => '#',
                'facebook' => '#',
                'pinterest' => '#',
                'instagram' => '#',
                'youtube' => '#',
                'created_at' => '2021-08-12 15:17:54',
                'updated_at' => '2021-08-12 15:17:54',
            ),
        ));
        
        
    }
}