<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ReviewsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('reviews')->delete();
        
        \DB::table('reviews')->insert(array (
            0 => 
            array (
                'id' => 1,
                'rating' => 5,
                'comment' => 'nice product',
                'order_item_id' => 2,
                'created_at' => '2021-10-05 20:43:47',
                'updated_at' => '2021-10-05 20:43:47',
            ),
            1 => 
            array (
                'id' => 2,
                'rating' => 4,
                'comment' => 'nice product 1',
                'order_item_id' => 2,
                'created_at' => '2021-10-05 20:43:55',
                'updated_at' => '2021-10-05 20:43:55',
            ),
        ));
        
        
    }
}