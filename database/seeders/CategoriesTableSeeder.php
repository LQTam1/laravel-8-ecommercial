<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('categories')->delete();
        
        \DB::table('categories')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'doloribus sed',
                'slug' => 'doloribus-sed',
                'created_at' => '2021-08-12 15:17:54',
                'updated_at' => '2021-08-12 15:17:54',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'quibusdam consequatur',
                'slug' => 'quibusdam-consequatur',
                'created_at' => '2021-08-12 15:17:54',
                'updated_at' => '2021-08-12 15:17:54',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'totam et',
                'slug' => 'totam-et',
                'created_at' => '2021-08-12 15:17:54',
                'updated_at' => '2021-08-12 15:17:54',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'ipsam praesentium',
                'slug' => 'ipsam-praesentium',
                'created_at' => '2021-08-12 15:17:54',
                'updated_at' => '2021-08-12 15:17:54',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'voluptatum illum',
                'slug' => 'voluptatum-illum',
                'created_at' => '2021-08-12 15:17:54',
                'updated_at' => '2021-08-12 15:17:54',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'culpa excepturi',
                'slug' => 'culpa-excepturi',
                'created_at' => '2021-08-12 15:17:54',
                'updated_at' => '2021-08-12 15:17:54',
            ),
        ));
        
        
    }
}