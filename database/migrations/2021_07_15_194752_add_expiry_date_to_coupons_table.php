<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class AddExpiryDateToCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('coupons', function (Blueprint $table) {
            // $table->date('expiry_date')->after('cart_value')->default(DB::raw('CURRENT_DATE'));
            $table->date('expiry_date')->after('cart_value');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('coupons', 'expiry_date')) {
            Schema::table('coupons', function (Blueprint $table) {
                $table->dropColumn('expiry_date');
            });
        }
    }
}
