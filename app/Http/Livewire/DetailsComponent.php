<?php

namespace App\Http\Livewire;

use App\Models\Product;
use App\Models\Sale;
use Livewire\Component;
use Cart;

class DetailsComponent extends Component
{
    public $slug;
    public $qty;

    public function mount($slug)
    {
        $this->slug = $slug;
        $this->qty = 1;
    }

    public function increaseQuantity()
    {
        $this->qty++;
    }

    public function decreaseQuantity()
    {
        if ($this->qty > 1)
            $this->qty--;
    }

    public function store($product_id, $product_name, $product_price)
    {
        Cart::instance('cart')->add($product_id, $product_name, $this->qty, $product_price)->associate('App\Models\Product');
        session()->flash('success_message', 'Item added in Cart');
        return redirect()->route('product.cart');
    }

    public function addToWishlist($product_id, $product_name, $product_price)
    {
        Cart::instance('wishlist')->add($product_id, $product_name, 1, $product_price)->associate('App\Models\Product');
        $this->emitTo('wishlist-count-component', 'refreshComponent');
        session()->flash('success_message', 'Product has been added to wishlist successfully!');
    }

    public function removeFromWishlist($product_id)
    {
        foreach (Cart::instance('wishlist')->content() as $item) {
            if ($item->id === $product_id) {
                Cart::instance('wishlist')->remove($item->rowId);
                $this->emitTo('wishlist-count-component', 'refreshComponent');
                session()->flash('success_message', 'Product has been removed from wishlist successfully!');
                return;
            }
        }
    }

    public function render()
    {
        $product = Product::with(['orderItems', 'orderItems.review', 'orderItems.order', 'orderItems.order.user'])->where('slug', $this->slug)->first();
        $orderItems = $product->orderItems->where('rvstatus', 1);
        $ratingArr = $orderItems->pluck('review.rating')->toArray();
        $avgRating = 0;
        if ($orderItems->count() > 0) {
            $avgRating = array_sum($ratingArr) / $orderItems->count();
        }
        $popular_products = Product::inRandomOrder()->limit(4)->get();
        $related_products = Product::where('category_id', $product->category_id)->inRandomOrder()->limit(5)->get();
        $images = explode(',', $product->images);
        $sale = Sale::find(1);

        return view('livewire.details-component', compact('product', 'popular_products', 'related_products', 'sale', 'avgRating', 'orderItems', 'images'))->layout('layouts.base');
    }
}
