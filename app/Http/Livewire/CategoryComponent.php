<?php

namespace App\Http\Livewire;

use App\Models\Category;
use App\Models\Product;
use Livewire\Component;
use Livewire\WithPagination;
use Cart;

class CategoryComponent extends Component
{
    use WithPagination;

    public $sorting;
    public $pagesize;
    public $category_slug;

    public function mount($category_slug)
    {
        $this->sorting = "default";
        $this->pagesize = 12;
        $this->category_slug = $category_slug;
    }

    public function store($product_id, $product_name, $product_price)
    {
        Cart::instance('cart')->add($product_id, $product_name, 1, $product_price)->associate('App\Models\Product');
        session()->flash('success_message', 'Item added in Cart');
        return redirect()->route('product.cart');
    }
    public function render()
    {
        $category = Category::where('slug', $this->category_slug)->first();
        $category_id = $category->id;
        $category_name = $category->name;

        switch ($this->sorting) {
            case 'date':
                $products = Product::where('category_id', $category_id)->orderBy('created_at', 'desc')->paginate($this->pagesize);
                break;
            case 'price':
                $products = Product::where('category_id', $category_id)->orderBy('regular_price', 'asc')->paginate($this->pagesize);
                break;

            case 'price-desc':
                $products = Product::where('category_id', $category_id)->orderBy('regular_price', 'desc')->paginate($this->pagesize);
                break;

            default:
                $products = Product::where('category_id', $category_id)->paginate($this->pagesize);
                break;
        }

        $categories = Category::all();
        return view('livewire.category-component', ['products' => $products, 'categories' => $categories, 'category_name' => $category_name])->layout('layouts.base');
    }
}
