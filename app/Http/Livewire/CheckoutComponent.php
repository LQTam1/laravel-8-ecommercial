<?php

namespace App\Http\Livewire;

use App\Events\OrderSucceededEvent;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Shipping;
use App\Models\Transaction;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Cart;
use Exception;
use Stripe;

class CheckoutComponent extends Component
{
    public $is_shipping_different;

    public $firstname;
    public $lastname;
    public $mobile;
    public $email;
    public $line1;
    public $line2;
    public $city;
    public $province;
    public $country;
    public $zipcode;

    public $shipping_firstname;
    public $shipping_lastname;
    public $shipping_mobile;
    public $shipping_email;
    public $shipping_line1;
    public $shipping_line2;
    public $shipping_city;
    public $shipping_province;
    public $shipping_country;
    public $shipping_zipcode;

    public $payment_method;
    public $thankyou;

    public $card_no;
    public $exp_month;
    public $exp_year;
    public $cvc;

    protected $billingRules = [
        'firstname' => 'required',
        'lastname' => 'required',
        'mobile' => 'required|numeric',
        'email' => 'required|email',
        'line1' => 'required',
        'city' => 'required',
        'province' => 'required',
        'country' => 'required',
        'zipcode' => 'required',
        'payment_method' => 'required',
    ];

    protected $shippingRules = [
        'shipping_firstname' => 'required',
        'shipping_lastname' => 'required',
        'shipping_mobile' => 'required|numeric',
        'shipping_email' => 'required|email',
        'shipping_line1' => 'required',
        'shipping_city' => 'required',
        'shipping_province' => 'required',
        'shipping_country' => 'required',
        'shipping_zipcode' => 'required',
    ];

    protected $cardRules = [
        'card_no' => 'required|numeric',
        'exp_month' => 'required|numeric',
        'exp_year' => 'required|numeric',
        'cvc' => 'required|numeric',
    ];

    public function updated($field)
    {
        $this->validateOnly($field, $this->billingRules);
        if ($this->is_shipping_different) {
            $this->validateOnly($field, $this->shippingRules);
        }

        if ($this->payment_method === 'card') {
            $this->validateOnly($field, $this->cardRules);
        }
    }
    public function placeOrder()
    {
        $this->validate($this->billingRules);
        if ($this->payment_method === 'card') {
            $this->validate($this->cardRules);
        }

        $order = new Order();
        $checkout = session()->get('checkout');

        $order->user_id = Auth::user()->id;
        $order->discount = $checkout['discount'];
        $order->tax = $checkout['tax'];
        $order->subtotal = $checkout['subtotal'];
        $order->total = $checkout['total'];

        $order->firstname = $this->firstname;
        $order->lastname = $this->lastname;
        $order->mobile = $this->mobile;
        $order->email = $this->email;
        $order->line1 = $this->line1;
        $order->line2 = $this->line2;
        $order->city = $this->city;
        $order->province = $this->province;
        $order->country = $this->country;
        $order->zipcode = $this->zipcode;
        $order->status = 'ordered';
        $order->is_shipping_different = $this->is_shipping_different ? 1 : 0;
        $order->save();

        foreach (Cart::instance('cart')->content() as $item) {
            $orderItem = new OrderItem();
            $orderItem->product_id =  $item->id;
            $orderItem->order_id = $order->id;
            $orderItem->price = $item->price;
            $orderItem->quantity = $item->qty;
            $orderItem->save();
        }

        if ($this->is_shipping_different) {
            $this->validate($this->shippingRules);
            $shipping = new Shipping();
            $checkout = session()->get('checkout');

            $shipping->order_id = $order->id;
            $shipping->firstname = $this->shipping_firstname;
            $shipping->lastname = $this->shipping_lastname;
            $shipping->mobile = $this->shipping_mobile;
            $shipping->email = $this->shipping_email;
            $shipping->line1 = $this->shipping_line1;
            $shipping->line2 = $this->shipping_line2;
            $shipping->city = $this->shipping_city;
            $shipping->province = $this->shipping_province;
            $shipping->country = $this->shipping_country;
            $shipping->zipcode = $this->shipping_zipcode;
            $shipping->save();
        }

        if ($this->payment_method === 'cod') {
            $this->makeTransaction($order->id, 'pending');
            event(new OrderSucceededEvent($order));
            $this->resetCart();
        } else if ($this->payment_method === 'card') {
            $stripe = Stripe::make(env('STRIPE_SECRET_KEY','STRIPE_SECRET_KEY'));
            try {
                $token = $stripe->tokens()->create([
                    'card' => [
                        'number' => $this->card_no,
                        'exp_month' => $this->exp_month,
                        'exp_year' => $this->exp_year,
                        'cvc' => $this->cvc
                    ]
                ]);

                if (!isset($token['id'])) {
                    session()->flash('stripe_error', 'The stripe token was not generated correctly!');
                    $this->thankyou = 0;
                }

                $customer = $stripe->customers()->create([
                    'name' => $this->firstname . ' ' . $this->lastname,
                    'email' => $this->email,
                    'phone' => $this->mobile,
                    'address' => [
                        'line1' => $this->line1,
                        'postal_code' => $this->zipcode,
                        'city' => $this->city,
                        'state' => $this->province,
                        'country' => $this->country
                    ],
                    'shipping' => [
                        'name' => $this->firstname . ' ' . $this->lastname,
                        'address' => [
                            'line1' => $this->line1,
                            'postal_code' => $this->zipcode,
                            'city' => $this->city,
                            'state' => $this->province,
                            'country' => $this->country
                        ],
                    ], 'source' => $token['id']
                ]);

                $charge = $stripe->charges()->create([
                    'customer' => $customer['id'],
                    'currency' => 'USD',
                    'amount' => session()->get('checkout')['total'],
                    'description' => 'Payment for order no.' . $order->id
                ]);

                if ($charge['status'] === 'succeeded') {
                    $this->makeTransaction($order->id, 'approved');
                    event(new OrderSucceededEvent($order));
                    $this->resetCart();
                } else {
                    session()->flash('stripe_error', 'Error in Transaction!');
                    $this->thankyou = 0;
                }
            } catch (Exception $e) {
                session()->flash('stripe_error', $e->getMessage());
                $this->thankyou = 0;
            }
        }
    }

    public function resetCart()
    {
        $this->thankyou = 1;
        Cart::instance('cart')->destroy();
        session()->forget('checkout');
        session()->flash('message', 'Order has been created successfully!');
    }
    public function makeTransaction($order_id, $status)
    {
        $transaction = new Transaction();
        $transaction->user_id = Auth::user()->id;
        $transaction->order_id = $order_id;
        $transaction->mode = $this->payment_method;
        $transaction->status = $status;
        $transaction->save();
    }
    public function verifyForCheckout()
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        } else if ($this->thankyou) {
            return redirect()->route('thankyou');
        } else if (!session()->get('checkout')) {
            return redirect()->route('product.cart');
        }
    }
    public function render()
    {
        $this->verifyForCheckout();
        return view('livewire.checkout-component')->layout('layouts.base');
    }
}
