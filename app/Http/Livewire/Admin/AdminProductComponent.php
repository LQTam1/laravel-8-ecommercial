<?php

namespace App\Http\Livewire\Admin;

use App\Models\Product;
use Livewire\Component;
use Livewire\WithPagination;

class AdminProductComponent extends Component
{
    use WithPagination;

    public $product_id = null;

    public function confirmDestroyProduct($id = null)
    {
        $this->product_id = $id;
    }

    public function destroy($id)
    {
        $product = Product::find($id);
        if ($product->image && is_file(Product::$FOLDER_TO_SAVE . '/' . $product->image)) {
            unlink(Product::$FOLDER_TO_SAVE . '/' . $product->image);
        }
        if ($product->images) {
            $images = explode(',', $product->images);
            foreach ($images as $image) {
                if ($image && is_file(Product::$FOLDER_TO_SAVE . '/' . $image)) {
                    unlink(Product::$FOLDER_TO_SAVE . '/' . $image);
                }
            }
        }
        $product->delete();
        session()->flash('message', 'Product has been deleted successfully!');
    }
    public function render()
    {
        $products = Product::with('category')->paginate(10);
        return view('livewire.admin.admin-product-component', compact('products'))->layout('layouts.base');
    }
}
