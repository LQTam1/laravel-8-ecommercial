<?php

namespace App\Http\Livewire\Admin;

use App\Models\Coupon;
use Livewire\Component;
use Livewire\WithPagination;

class AdminCouponComponent extends Component
{
    use WithPagination;

    public function destroy($id)
    {
        Coupon::destroy($id);
        session()->flash('message', 'Coupon has been deleted successfully!');
    }
    public function render()
    {
        $coupons = Coupon::paginate(10);
        return view('livewire.admin.admin-coupon-component', compact('coupons'))->layout('layouts.base');
    }
}
