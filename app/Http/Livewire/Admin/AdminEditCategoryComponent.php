<?php

namespace App\Http\Livewire\Admin;

use App\Models\Category;
use Livewire\Component;
use Illuminate\Support\Str;

class AdminEditCategoryComponent extends Component
{
    public $category_slug;
    public $category_id;
    public $name;
    public $slug;

    protected $rules = [
        'name' => 'required', 'slug' => 'required|unique:categories'
    ];
    public function mount($category_slug)
    {
        $this->category_slug = $category_slug;
        $category = Category::where('slug', $category_slug)->first();
        $this->category_id = $category->id;
        $this->name = $category->name;
        $this->slug = $category->slug;
    }

    public function updated($fields)
    {
        $this->rules['slug'] = $this->rules['slug'] . ',slug,' . $this->category_id;
        $this->validateOnly($fields, $this->rules);
    }
    public function generateSlug()
    {
        $this->slug = Str::slug($this->name);
    }

    public function update()
    {
        $this->rules['slug'] = $this->rules['slug'] . ',slug,' . $this->category_id;
        $this->validate();
        $category = Category::find($this->category_id);
        $category->name = $this->name;
        $category->slug = $this->slug;
        $category->save();
        session()->flash('message', 'Category has been updated successfully!');
    }
    public function render()
    {
        return view('livewire.admin.admin-edit-category-component')->layout('layouts.base');
    }
}
